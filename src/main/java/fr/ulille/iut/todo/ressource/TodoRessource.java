package fr.ulille.iut.todo.ressource;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import fr.ulille.iut.todo.dto.CreationTacheDTO;
import fr.ulille.iut.todo.service.Tache;
import fr.ulille.iut.todo.service.TodoService;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.EntityTag;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

@Path("taches")
@Produces(MediaType.APPLICATION_JSON)
public class TodoRessource {
    private final static Logger LOGGER = Logger.getLogger(TodoRessource.class.getName());

    private TodoService todoService = new TodoService();

    @Context
    private UriInfo uri;
    @Context
    Request request;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<Tache> getAll() {
        LOGGER.info("getAll()");

        return todoService.getAll();
    }

    @POST
    public Response createTache(CreationTacheDTO tacheDto) {
        LOGGER.info("createTache()");

        Tache tache = Tache.fromCreationTacheDTO(tacheDto);
        todoService.addTache(tache);
        URI location = uri.getAbsolutePathBuilder().path(tache.getId().toString()).build();

        EntityTag etag = new EntityTag(Integer.toString(tache.hashCode()));
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        if (builder == null) {
            builder = Response.created(location);
            builder.tag(etag);
            builder.entity(tache);
        }
        return builder.build();
    }
    
    @GET
    @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Tache getById(@PathParam("id")UUID id) {
    	Tache test=todoService.getTache(id);
    	if(test == null) {
    		throw new WebApplicationException(404);
    	}
    	return test;
    }
    
    @GET
    @Path("{id}/description")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public String getDescription(@PathParam("id")UUID id) {
    	String test=todoService.getTache(id).getDescription();
    	if(test == null) {
    		throw new WebApplicationException(404);
    	}
    	return test;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createTacheFromForm(@FormParam("nom") String nom, @FormParam("description") String description) {
    	LOGGER.info("createTacheFromForm()");

        Tache task = new Tache();
        task.setNom(nom);
        task.setDescription(description);
        todoService.addTache(task);
        URI location = uri.getAbsolutePathBuilder().path(task.getId().toString()).build();

        EntityTag etag = new EntityTag(Integer.toString(task.hashCode()));
        ResponseBuilder build = request.evaluatePreconditions(etag);

        if (build == null) {
            build = Response.created(location);
            build.tag(etag);
            build.entity(task);
        }
        return build.build();
    }
    
    @DELETE
    @Path("{id}")
    public Response deleteTask(@PathParam("id")UUID id) {
    	int test=todoService.deleteTache(id.toString());
    	if(test==0) {
    		throw new WebApplicationException(404);
    	}
    	return Response.status(204).build();
    }
    
    @PUT
    @Path("{id}")
    public Response putTache(CreationTacheDTO tacheDto) {
        LOGGER.info("putTache()");
        if(tacheDto.getDescription()==null || tacheDto.getNom()==null) {
        	throw new WebApplicationException(400);
        }
        Tache task = Tache.fromCreationTacheDTO(tacheDto);
        todoService.updateTache(task);
        return Response.accepted(Entity.json(task)).build();
    }
    

}